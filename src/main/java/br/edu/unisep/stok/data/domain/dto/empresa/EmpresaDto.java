package br.edu.unisep.stok.data.domain.dto.empresa;


import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class EmpresaDto {

    private Integer id;
    private String nome;
    private String razaoSocial;

}
