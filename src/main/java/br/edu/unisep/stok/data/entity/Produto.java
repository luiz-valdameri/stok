package br.edu.unisep.stok.data.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "produto")
public class Produto {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "valor")
    private Double valor;

    @Column(name = "nome")
    private String nome;

    @ManyToOne
    @JoinColumn(name="empresa_id", nullable=false)
    private Empresa empresa;

}
