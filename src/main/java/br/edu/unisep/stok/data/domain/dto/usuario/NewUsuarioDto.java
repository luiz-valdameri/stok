package br.edu.unisep.stok.data.domain.dto.usuario;

import br.edu.unisep.stok.data.entity.Empresa;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class NewUsuarioDto {

    private Double senha;
    private String nome;
    private String login;
    private Integer empresa;

}
