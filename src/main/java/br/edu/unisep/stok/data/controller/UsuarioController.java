package br.edu.unisep.stok.data.controller;

import br.edu.unisep.stok.data.domain.dto.usuario.NewUsuarioDto;
import br.edu.unisep.stok.data.domain.usecase.usuario.FindAllUsuariosUseCase;
import br.edu.unisep.stok.data.domain.usecase.usuario.SaveUsuarioUseCase;
import br.edu.unisep.stok.data.entity.Usuario;
import lombok.AllArgsConstructor;
import org.apache.catalina.connector.Response;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/usuario")
public class UsuarioController {

    private FindAllUsuariosUseCase findAllUseCase;
    private SaveUsuarioUseCase saveUseCase;

    @GetMapping
    public ResponseEntity<List<Usuario>> findAll() {
        List<Usuario> usuarios = findAllUseCase.execute();

        if (usuarios.isEmpty()) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(usuarios);
    }

    @PostMapping
    public Integer save(@RequestParam("usuario") NewUsuarioDto newUsuario) {
        saveUseCase.execute(newUsuario);

        return Response.SC_CREATED;
    }

}
