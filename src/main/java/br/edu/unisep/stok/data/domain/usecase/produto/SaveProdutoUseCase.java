package br.edu.unisep.stok.data.domain.usecase.produto;

import br.edu.unisep.stok.data.domain.dto.produto.NewProdutoDto;
import br.edu.unisep.stok.data.entity.Empresa;
import br.edu.unisep.stok.data.entity.Produto;
import br.edu.unisep.stok.data.repository.ProdutoRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class SaveProdutoUseCase {

    private ProdutoRepository repository;

    public void execute(NewProdutoDto newProduto) {
        Produto produto = new Produto();
        produto.setNome(newProduto.getNome());
        produto.setValor(newProduto.getValor());

        Empresa empresa = new Empresa();
        empresa.setId(newProduto.getEmpresa());
        produto.setEmpresa(empresa);

        repository.save(produto);
    }


}
 