package br.edu.unisep.stok.data.domain.dto.produto;

import br.edu.unisep.stok.data.entity.Empresa;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class NewProdutoDto {

    private Double valor;
    private String nome;
    private Integer empresa;

}
