package br.edu.unisep.stok.data.repository;

import br.edu.unisep.stok.data.entity.Produto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProdutoRepository extends JpaRepository<Produto, Integer> {

    @Query("FROM Produto ORDER BY nome ASC ")
    List<Produto> list();

}
