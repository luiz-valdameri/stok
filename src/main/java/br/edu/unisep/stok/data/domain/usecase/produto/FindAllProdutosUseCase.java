package br.edu.unisep.stok.data.domain.usecase.produto;

import br.edu.unisep.stok.data.entity.Produto;
import br.edu.unisep.stok.data.repository.ProdutoRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class FindAllProdutosUseCase {

    private ProdutoRepository repository;

    public List<Produto> execute() {
        return repository.list();
    }

}
