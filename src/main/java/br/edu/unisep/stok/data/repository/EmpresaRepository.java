package br.edu.unisep.stok.data.repository;

import br.edu.unisep.stok.data.entity.Empresa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmpresaRepository extends JpaRepository<Empresa, Integer> {

    @Query("FROM Empresa ORDER BY nome ASC ")
    List<Empresa> list();

}
