package br.edu.unisep.stok.data.domain.usecase.empresa;

import br.edu.unisep.stok.data.entity.Empresa;
import br.edu.unisep.stok.data.repository.EmpresaRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class FindAllEmpresasUseCase {

    private EmpresaRepository repository;

    public List<Empresa> execute() {
        return repository.list();
    }

}
