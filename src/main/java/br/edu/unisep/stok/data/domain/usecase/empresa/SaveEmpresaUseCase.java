package br.edu.unisep.stok.data.domain.usecase.empresa;

import br.edu.unisep.stok.data.domain.dto.empresa.NewEmpresaDto;
import br.edu.unisep.stok.data.entity.Empresa;
import br.edu.unisep.stok.data.repository.EmpresaRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class SaveEmpresaUseCase {

    private EmpresaRepository repository;

    public void execute(NewEmpresaDto newEmpresa) {
        Empresa empresa = new Empresa();
        empresa.setNome(newEmpresa.getNome());
        empresa.setRazaoSocial(newEmpresa.getRazaoSocial());

        repository.save(empresa);
    }

}
