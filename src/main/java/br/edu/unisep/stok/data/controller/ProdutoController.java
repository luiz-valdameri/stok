package br.edu.unisep.stok.data.controller;

import br.edu.unisep.stok.data.domain.dto.produto.NewProdutoDto;
import br.edu.unisep.stok.data.domain.usecase.produto.FindAllProdutosUseCase;
import br.edu.unisep.stok.data.domain.usecase.produto.SaveProdutoUseCase;
import br.edu.unisep.stok.data.entity.Produto;
import lombok.AllArgsConstructor;
import org.apache.catalina.connector.Response;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/produto")
public class ProdutoController {

    private FindAllProdutosUseCase findAllUseCase;
    private SaveProdutoUseCase saveUseCase;

    @GetMapping
    public ResponseEntity<List<Produto>> findAll() {
        List<Produto> produtos = findAllUseCase.execute();

        if (produtos.isEmpty()) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(produtos);
    }

    @PostMapping("/criar")
    public Integer save(@RequestBody NewProdutoDto produto) {
        saveUseCase.execute(produto);

        return Response.SC_CREATED;
    }

}
