package br.edu.unisep.stok.data.domain.usecase.usuario;

import br.edu.unisep.stok.data.entity.Usuario;
import br.edu.unisep.stok.data.repository.UsuarioRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class FindAllUsuariosUseCase {

    private UsuarioRepository repository;

    public List<Usuario> execute() {
        return repository.list();
    }

}
