package br.edu.unisep.stok.data.domain.usecase.usuario;

import br.edu.unisep.stok.data.domain.dto.usuario.NewUsuarioDto;
import br.edu.unisep.stok.data.entity.Empresa;
import br.edu.unisep.stok.data.entity.Usuario;
import br.edu.unisep.stok.data.repository.UsuarioRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class SaveUsuarioUseCase {

    private UsuarioRepository repository;

    public void execute(NewUsuarioDto newUsuario) {
        Usuario usuario = new Usuario();
        usuario.setNome(newUsuario.getNome());
        usuario.setLogin(newUsuario.getLogin());
        usuario.setSenha(newUsuario.getSenha());

        Empresa empresa = new Empresa();
        empresa.setId(newUsuario.getEmpresa());
        usuario.setEmpresa(empresa);

        repository.save(usuario);
    }

}
