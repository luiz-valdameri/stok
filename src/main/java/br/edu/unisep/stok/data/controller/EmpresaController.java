package br.edu.unisep.stok.data.controller;

import br.edu.unisep.stok.data.domain.dto.empresa.NewEmpresaDto;
import br.edu.unisep.stok.data.domain.usecase.empresa.FindAllEmpresasUseCase;
import br.edu.unisep.stok.data.domain.usecase.empresa.SaveEmpresaUseCase;
import br.edu.unisep.stok.data.entity.Empresa;
import lombok.AllArgsConstructor;
import org.apache.catalina.connector.Response;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/empresa")
public class EmpresaController {

    private FindAllEmpresasUseCase findAllUseCase;
    private SaveEmpresaUseCase saveUseCase;

    @GetMapping
    public ResponseEntity<List<Empresa>> findAll() {
        List<Empresa> empresas = findAllUseCase.execute();

        if (empresas.isEmpty()) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(empresas);
    }

    @PostMapping("/criar")
    public ResponseEntity<NewEmpresaDto> save(@RequestBody NewEmpresaDto empresa) {
        saveUseCase.execute(empresa);

        return ResponseEntity.ok(empresa);
    }

}
