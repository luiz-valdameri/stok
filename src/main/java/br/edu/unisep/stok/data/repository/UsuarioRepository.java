package br.edu.unisep.stok.data.repository;

import br.edu.unisep.stok.data.entity.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, Integer> {

    @Query("FROM Usuario ORDER BY nome ASC ")
    List<Usuario> list();

}
